
//EIGENVAECTOR

public class tree{
  public static void main(String[] args) {
    long param1 = Integer.parseInt(args[0]);
    long param2 = Integer.parseInt(args[1]);
    long startTime = System.nanoTime();
    String res = solution(param1,param2);
    long endTime = System.nanoTime();
    long ms = (endTime - startTime) / 1000000;

    System.out.println(res);
    System.out.println("time = " + ms + " ms");
  }
  
  public static String solution(long x, long y) {
    
    int level = 0,max = 200001;
    long cell_x = 0;
    long[] axis_y = new long[max];
    
    while(level != max){
      cell_x =  cell_x + level;
      axis_y[level] = cell_x - (level - 1);
      level++;
    }

    int notch = (int)x + (int)y - 1;
    long yval = axis_y[notch]; 
    long ans = yval + (int)x - 1;
    return "" + ans;
  }

}