

import java.util.ArrayList;
import java.util.Arrays;

public class chess{

  public static void main(String[] args) {
    int param1 = Integer.parseInt(args[0]);
    int param2 = Integer.parseInt(args[1]);
    long startTime = System.nanoTime();
    int res = solution(param1,param2);
    long endTime = System.nanoTime();
    long ms = (endTime - startTime) / 1000000;

    System.out.println("Tile " + param2 + " is reached. Move count: " + res);
    System.out.println("time = " + ms + " ms");
  }

  public static int solution(int src, int dest) {

    ArrayList<int[]> movement_tree = new ArrayList<int[]>();
    Boolean[][] dorm_floor = new Boolean[8][8];

    int[] KY = { 1, 2, 2, 1, -1, -2, -2, -1 }, KX = { 2, 1, -1, -2, -2, -1, 1, 2 };
    int[] TY = { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1,2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3,  4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5,6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7}, TX = { 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7};

    int[] starting_tile = {src};
    movement_tree.add(0, starting_tile);

    for (Boolean[] row : dorm_floor){
      Arrays.fill(row, false);
    }
    
    int level = 0;
    
    while(dorm_floor[TY[dest]][TX[dest]] == false){
      for (int m = 0; m < movement_tree.get(level).length; m++) {
        int[] possible_moves = new int[8];
        int movement_Y = TY[movement_tree.get(level)[m]];
        int movement_X = TX[movement_tree.get(level)[m]];
        dorm_floor[movement_Y][movement_X] = true;
       
        for (int km = 0; km < 8; km++) {
          int new_Y = movement_Y + KY[km], new_X = movement_X + KX[km];
          if(new_Y < 0 || new_Y > 7 || new_X < 0 || new_X > 7 || dorm_floor[new_Y][new_X] == true){
            possible_moves[km] = -1;
            continue;
          }
          possible_moves[km] = (new_Y * 8) + new_X;
        }

        int[] new_moves = Arrays.stream(possible_moves).filter(x -> x > -1).toArray();

        if(m==0){
          int[] init_t = {-1};
          movement_tree.add(level + 1, init_t);
        }
        
        int[] existing_moves = Arrays.stream(movement_tree.get(level + 1)).filter(x -> x > -1).toArray();
        int[] result = Arrays.copyOf(new_moves, new_moves.length + existing_moves.length);
        System.arraycopy(existing_moves, 0, result, new_moves.length, existing_moves.length);

        movement_tree.add(level + 1, result);
      }
    
      level++;
    }
  
    return level - 1;
  }
}