import java.util.stream.IntStream;
import java.util.Arrays;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class fuel{
  public static void main(String[] args) {
    long startTime = System.nanoTime();
    int[][] test = { 
      {0,1,0,0,0,1}, 
      {4,0,0,3,2,0}, 
      {0,0,0,0,0,0}, 
      {0,0,0,0,0,0}, 
      {0,0,0,0,0,0},
      {0,0,0,0,0,0}};
    solution(test);
    //Output: [0, 3, 2, 9, 14].
    long endTime = System.nanoTime();
    long ms = (endTime - startTime) / 1000000;
    System.out.println("time = " + ms + " ms");
  }
  
  public static int[] solution(int[][] m) {
    double[][] matrix = convertIntToDouble(m);
    boolean[] terminal_states = getTerminalStates(m);
    double[] stationary_distribution = getStationaryDistribution(matrix);
 
    for (double column : stationary_distribution) {  System.out.print( round(column) + "  "  );  }
    System.out.println("");

    for(int i = 0;  i < terminal_states.length; i++){
      System.out.print((terminal_states[i])? "S" + i + ","  : "" );
    }
    System.out.println("");

    int[] res = {1,1,1,1,1};
    return res;
  }

  public static boolean[] getTerminalStates(int[][] m) {
    boolean[] result = new boolean[m.length];
    for(int i = 0;  i < m.length; i++){
      int sum = IntStream.of(m[i]).sum();
      result[i] = (sum == 0);
    }
    return result;
  }
  
  public static double[][] convertIntToDouble(int[][] m) {
    double[][] result = new double[m.length][m.length];
    for(int i = 0;  i < m.length; i++){
      int sum = IntStream.of(m[i]).sum();
      for(int si = 0;  si < m.length; si++){
        result[i][si] = (sum > 0)? (double) m[i][si] / sum : 0;
      }
    }
    return result;
  }

  public static double[] getStationaryDistribution(double[][] m) {
    double[] row_vector = new double[m.length], result = new double[m.length];
    row_vector[0] = 1;
    boolean match = false;
    double[] last_row_vector = row_vector;
    while(match != true){
      row_vector = multiplyVectorToMatrix(row_vector, m);
      if( Arrays.equals(last_row_vector, row_vector)){ match = true; continue; }
      last_row_vector = row_vector;
      for(int i = 0; i < row_vector.length; i++){
        result[i]  +=  row_vector[i];
      } 
    }
    return result;
  }

  public static double[] multiplyVectorToMatrix(double[] vector, double[][] m) {
    int column = 0;
    double[] result = new double[ vector.length];
    while(column < vector.length){
      double product = 0;
      for(int row_index = 0;  row_index < m.length; row_index++){
        double vm = vector[row_index] *  m[row_index][column];
        product = product + vm;
      }
      result[column] =product; 
      column++;
    }
    return result;
  }
  public static double round(double num) {
      BigDecimal res = BigDecimal.valueOf(num);
      res = res.setScale(4, RoundingMode.HALF_UP);
      return res.doubleValue();
  }
}