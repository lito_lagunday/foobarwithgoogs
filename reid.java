


public class reid{
  public static void main(String[] args){

    int param1 = Integer.parseInt(args[0]);

    long startTime = System.nanoTime();
    String res = solution(param1);
    long endTime = System.nanoTime();
    long ms = (endTime - startTime) / 1000000;

    System.out.println(res);
    System.out.println("time = " + ms + " ms");
  }
  
  public static String solution(int x) {

    int i = x, upper = 20300;
    boolean num_set[] =  new boolean[upper + 1];
    String prime_id_string = "";

    for (int num = 2; num <= upper; num++){
      num_set[num] = true;
    }

    for (int num = 2; num * num <= upper; num++){
      if (num_set[num] == true){
        for (int index = num * num; index <= upper; index += num){
          num_set[index] = false;
        }
      }
    }

    for (int num = 2; num  <= upper; num++){
      if (num_set[num] == true){        
        prime_id_string += Integer.toString(num);
      }
    }

    String minion_id = prime_id_string.substring(i,i + 5);
    
    return minion_id;
  }
}